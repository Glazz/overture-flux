var classRobot =
[
    [ "AutonomousInit", "classRobot.html#a2136cfc015936285218c8a8db984d6bc", null ],
    [ "AutonomousPeriodic", "classRobot.html#ac11143dd674e0e02fef5329e2df24830", null ],
    [ "RobotInit", "classRobot.html#a66f23dae271748d525cf3ab046375f79", null ],
    [ "TeleopInit", "classRobot.html#aa3e246794bfbbb4406fc87f351762038", null ],
    [ "TeleopPeriodic", "classRobot.html#a324322627c63b3870daf7c7ddc5bea63", null ],
    [ "TestInit", "classRobot.html#a9ac222d45d30a6d0c572fd36d18c6ccc", null ],
    [ "TestPeriodic", "classRobot.html#af0ac44a962e609e9b042285e699d1db8", null ]
];