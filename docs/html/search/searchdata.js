var indexSectionsWithContent =
{
  0: "abcdfghijmnoprst",
  1: "cdfghoprs",
  2: "cdfghmoprs",
  3: "abcdfghijmnoprst",
  4: "m",
  5: "g",
  6: "fs"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator"
};

